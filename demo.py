import os

import flask


app = flask.Flask(__name__)

TEMPLATE = """
<html>
Hello {{ name }}!
</html>
"""


@app.route("/unsafe")
def unsafe():
    name = flask.request.args.get('name')
    return flask.render_template_string(
        TEMPLATE,
        name=name
    )


@app.route("/safe")
def safe():
    name = flask.request.args.get('name')
    return flask.render_template_string(
        TEMPLATE,
        name=flask.Markup.escape(name)
    )


if __name__ == '__main__':
    app.debug = os.getenv("FLASK_DEBUG") == "1"
    if app.debug:
        print("WARNING: DEBUG MODE IS ENABLED!")
    app.config["PROPAGATE_EXCEPTIONS"] = True
    app.run(threaded=True)
